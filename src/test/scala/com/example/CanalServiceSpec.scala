//#full-example
package com.example

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.{TestKit, TestProbe}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

//#test-classes
class CanalServiceSpec(_system: ActorSystem)
  extends TestKit(_system)
    with Matchers
    with WordSpecLike
    with BeforeAndAfterAll {
  //#test-classes

  def this() = this(ActorSystem("CanalServiceSpec"))

  override def afterAll: Unit = {
    shutdown(system)
  }

  "A Canal Service Actor" should {
    "preprocess the bytestring in a correct way" in {
      implicit val system = ActorSystem("test")
      val settings = ActorMaterializerSettings(system).withDebugLogging(true)
      implicit val materializer = ActorMaterializer(settings, "test")
      val row1 = ByteString("tt0016672\tmovie\tThe Boob\tThe Boob\t0\t1926\t\\N\t64\tComedy,Romance")
      val row2 = ByteString("tt0000003\tshort\tPauvre Pierrot\tPauvre Pierrot\t0\t1892\t\\N\t4\tAnimation,Comedy,Romance")
      val row3 = ByteString("tt0000003\tshort\tPauvre Pierrot\tPauvre Pierrot\t0\t1892\t\\N\t4\tAnimation,Comedy,Romance")
      val row4 = ByteString("tt0000004\tshort\tUn bon bock\tUn bon bock\t0\t1892\t\\N\t\\N\tAnimation,Short")
      val row5 = ByteString("tt0000001\tshort\tCarmencita\tCarmencita\t0\t1894\t\\N\t1\tDocumentary,Short")
      val source = Source(List(row1, row2, row3, row4, row5))

      val service = new CanalService()
      val res: Seq[ByteString] = Await.result(
        source.via(service.getFlow).runWith(Sink.fold(Seq[ByteString]())((old, newE) => {
          old :+ newE
        }))
        , 2.seconds)
      res.size shouldBe 1
      res(0) shouldBe ByteString("The Boob")
    }
  }

}
