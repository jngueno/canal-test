//#full-example
package com.example

import java.nio.file.Paths

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.alpakka.amqp._
import akka.stream.scaladsl.{FileIO, Flow, Sink, Source}
import akka.stream.{ActorMaterializer, IOResult}
import akka.util.ByteString

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object CanalTest extends App {

  implicit val system: ActorSystem = ActorSystem()

  import system.dispatcher

  implicit val flowMaterializer: ActorMaterializer = ActorMaterializer()

  val connectionProvider = AmqpLocalConnectionProvider

  val queueName = "amqp-canal-queue-" + System.currentTimeMillis()
  val queueDeclaration = QueueDeclaration(queueName)

  val logFile = Paths.get("src/main/resources/data.tsv")

  val source: Source[ByteString, Future[IOResult]] = FileIO.fromPath(logFile)

  val flow: Flow[ByteString, ByteString, NotUsed] = new CanalService().getFlow

  source
    .via(flow)
    .runWith(AmqpRepository.amqpSink)
    .andThen {
      case _ =>
        system.terminate()
        Await.ready(system.whenTerminated, 1 minute)
    }

  // Récupération des 10 premiers messages
  val result = AmqpRepository.amqpSource.take(10).runWith(Sink.seq)
  result.map({ ex =>
    for (incMess <- ex) println(incMess.bytes.utf8String)
  })
}
