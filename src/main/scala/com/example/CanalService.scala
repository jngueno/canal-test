package com.example

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Framing}
import akka.util.ByteString
import com.example.model.BasicsModel

class CanalService() {
  val getFlow: Flow[ByteString, ByteString, NotUsed] = Framing
    .delimiter(ByteString("\n"), 500, true)
    .map(_.utf8String)
    .map { line => {
      val lineArr = line.toString.split("\t")
      val newBasics = BasicsModel(lineArr(1), lineArr(3), lineArr(8).split(","))
      newBasics
    }
    }
    .filter(basic => basic.titleType == "movie" && basic.genres.contains("Comedy"))
    .map(basic => ByteString(basic.originalTitle))
}
