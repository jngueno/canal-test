package com.example.model


case class BasicsModel(titleType: String,
                        originalTitle: String,
                        genres: Array[String])