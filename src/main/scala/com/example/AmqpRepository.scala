package com.example

import akka.{Done, NotUsed}
import akka.stream.alpakka.amqp.{AmqpSinkSettings, IncomingMessage, NamedQueueSourceSettings}
import akka.stream.alpakka.amqp.scaladsl.{AmqpSink, AmqpSource}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.example.CanalTest.{connectionProvider, queueDeclaration, queueName}

import scala.concurrent.Future

object AmqpRepository {

  def amqpSink: Sink[ByteString, Future[Done]] = {
    AmqpSink.simple(
      AmqpSinkSettings(connectionProvider)
        .withRoutingKey(queueName)
        .withDeclaration(queueDeclaration)
    )
  }

  def amqpSource: Source[IncomingMessage, NotUsed] =  AmqpSource.atMostOnceSource(
    NamedQueueSourceSettings(connectionProvider, queueName).withDeclaration(queueDeclaration),
    bufferSize = 10
  )
}
