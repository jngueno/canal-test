name := "canal-test"

version := "1.0"

scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.20"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % "1.0-M2"
)
